export function getCurveLenght(curve) {
    if(curve.type == 'line'){
        return lineLenght(curve.start, curve.end);
    } else {
        return quadraticBezierLength(curve.start, curve.end, curve.curveTop);
    }
}


export function quadraticBezierLength(p0, p1, p2) {
    var ax = p0[0] - 2 * p1[0] + p2[0];
    var ay = p0[1] - 2 * p1[1] + p2[1];
    var bx = 2 * p1[0] - 2 * p0[0];
    var by = 2 * p1[1] - 2 * p0[1];
    var A = 4 * (ax * ax + ay * ay);
    var B = 4 * (ax * bx + ay * by);
    var C = bx * bx + by * by;

    var Sabc = 2 * Math.sqrt(A+B+C);
    var A_2 = Math.sqrt(A);
    var A_32 = 2 * A * A_2;
    var C_2 = 2 * Math.sqrt(C);
    var BA = B / A_2;

    return (A_32 * Sabc + A_2 * B * (Sabc - C_2) + (4 * C * A - B * B) * Math.log((2 * A_2 + BA + Sabc) / (BA + C_2))) / (4 * A_32);
}

export function lineLenght(p0, p1){
    const v1 = p1[0]-p0[0];
    const v2 = p1[1]-p0[1];
    return Math.sqrt(v1*v1 + v2*v2);
}

export function getLineXYatPercent(start, end, percent) {
    var dx = end[0] - start[0];
    var dy = end[1] - start[1];
    var X = start[0] + dx * percent;
    var Y = start[1] + dy * percent;
    return ({
        x: X,
        y: Y
    });
}

export function getQuadraticBezierXYatPercent(start, control, end, percent) {
    var x = Math.pow(1 - percent, 2) * start[0] + 2 * (1 - percent) * percent * control[0] + Math.pow(percent, 2) * end[0];
    var y = Math.pow(1 - percent, 2) * start[1] + 2 * (1 - percent) * percent * control[1] + Math.pow(percent, 2) * end[1];
    return ({
        x: x,
        y: y
    });
}

export function normalize(v1, v2){
    const vectorSize = Math.sqrt(v1*v1 + v2*v2);
    return [v1/vectorSize, v2/vectorSize]
}

export function getAngle(v1, v2){
    var cross = v1[0]*v2[1] - v1[1]*v2[0];
    var dot = v1[0]*v2[0] - v1[1]*v2[1];
    return Math.atan2(cross, dot);
}