import 'styles/index.scss';
import Simulation from './Simulation';

window.onload = ()=>{
    var canvas = document.getElementById('canvas');
    canvas.style.backgroundColor = '#ddd';
    new Simulation(canvas);
};



