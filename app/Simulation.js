import { getLineXYatPercent, getQuadraticBezierXYatPercent, getCurveLenght, lineLenght, normalize} from './Mathematics';
import { roadData } from './../data/curveData.json';
import { backTrajectoryData , trajectoryData, distances} from './../data/trajectory.json';

export default class Simulation {
    constructor(canvas){
        this.context = canvas.getContext("2d");
        this.width = canvas.width;
        this.height = canvas.height;
        this.drawArea = this.drawArea.bind(this);
        this.drawArea();
    }

    drawArea() {
        this.context.clearRect(0, 0, this.width, this.height);
        this.context.lineWidth = 2;
        this.context.setLineDash([5, 15]);
        this.context.strokeStyle = 'black';
        const qx = 5;
        const qy = 5; 
        
        roadData.forEach((item)=>{
            if(item.type == 'line'){
                this.drawLine([item.start[0]*qx, item.start[1]*qy], [item.end[0]*qx, item.end[1]*qy]);
            } else if(item.type == 'circle') {
                this.drawCircle(item.center[0]*qx, item.center[1]*qx, item.radius*qx, item.start, item.end);
            }
        });
        
        if(trajectoryData) this.drawExternTrajectory(trajectoryData, '#FF0000');
        if(backTrajectoryData) this.drawExternTrajectory(backTrajectoryData, '#0000FF', true);
    }

    drawExternTrajectory (trajectory, color, back) {
        const qx = 5;
        this.context.beginPath();
        this.context.setLineDash([1, 0]);
        this.context.strokeStyle=color;
        trajectory.forEach((item, index)=>{
            this.context.moveTo(item[0]*qx, item[1]*qx);
            let nextItem = trajectory[index + 1];
            let frontPoint = trajectoryData[index + 1];
            if(nextItem){
                const bububu = [nextItem[0]*qx, nextItem[1]*qx];
                this.context.lineTo(...bububu);
                if(!back && (index % 3 == 0)){
                    const v = normalize(item[0]-nextItem[0], item[1]-nextItem[1]);
                    const n = [v[1], -v[0]];
                    this.context.font = "12px Arial";
                    const text = Math.round(distances[index+1] * 100) / 100;
                    this.context.fillText(text ,(nextItem[0]-n[0]*15)*qx, (nextItem[1]-n[1]*15)*qx);
                }
            }
            if(back && nextItem){
                const v = normalize(frontPoint[0]-nextItem[0], frontPoint[1]-nextItem[1]);
                const n = [v[1], -v[0]];
                const x = nextItem[0] + v[0]*44.9 + n[0]*10.3;
                const y = nextItem[1] + v[1]*44.9 + n[1]*10.3;
                this.context.moveTo(x *qx, y * qx);
                this.context.lineTo((x-n[0]*20.6)*qx, (y-n[1]*20.6)*qx);
                
            }
        });
        
        this.context.closePath();
        this.context.stroke();

    }

    drawLine(start, end) {
        this.context.beginPath();
        this.context.moveTo(...start);
        this.context.lineTo(...end);
        this.context.lineCap="round";
        this.context.stroke();
    }

    drawCurve(start, end, curveTop) {
        this.context.beginPath();
        this.context.moveTo(...start);
        this.context.quadraticCurveTo(...curveTop, ...end);
        this.context.lineCap="round";
        this.context.stroke();
    }

    drawCircle(...props){
        this.context.beginPath();
        this.context.arc(...props);
        this.context.lineCap="round";
        this.context.stroke();
    }


}