## Requirements
You only need <b>node.js</b> and <b>Python</b> pre-installed and you’re good to go. 

## Setup
Install dependencies
```sh
$ npm install
```

## Development
* Run the local webpack-dev-server with livereload and autocompile on [http://localhost:8080/](http://localhost:8080/)
```sh
$ npm start
```

* Edit road data in `data/curveData.json`
* Edit simulation parameters in `trajectoryEvolution/config.json`
* Generate trajectory and save data to `data/trajectory.json`
```sh
$ cd trajectoryEvolution && evolution.py
```

## [webpack](https://webpack.js.org/)
If you're not familiar with webpack, the [webpack-dev-server](https://webpack.js.org/configuration/dev-server/) will serve the static files in your build folder and watch your source files for changes.
When changes are made the bundle will be recompiled. This modified bundle is served from memory at the relative path specified in publicPath.
