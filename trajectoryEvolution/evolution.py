import random
import math
import json
import sys
import os
import signal
import time

current_file_dir = os.path.dirname(__file__)
data = json.load(open('./../data/curveData.json'))
roadData = data["roadData"]
startDir = data["startDir"]
startPoint = data["startPoint"]

config = json.load(open('config.json'))
populationSize = config["populationSize"]
generationCount = config["generationCount"]
trajectoryLenght = config["trajectoryLength"]
speed = config["speed"]
wheelDistance = config["wheelDistance"]
deltaTime = config["deltaTime"]
pCrossOver = config["pCrossOver"]
pMutation = config["pMutation"]
mutationRateStart = config["mutationRateStart"]
mutationRateEnd = config["mutationRateEnd"]
mutationRate = mutationRateStart

def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    fitterstScore = fitScores.index(min(fitScores))
    frontTraj, backTraj, distances = getTrajectoryPoints(population[fitterstScore])

    print('Best fit:', min(fitScores))
    with open('./../data/trajectory.json', 'w') as outfile:
        json.dump({"trajectoryData": frontTraj, "backTrajectoryData": backTraj, "distances": distances}, outfile)

    with open('./../data/scores.json', 'w') as outfile:
        json.dump({"scores": bestScores}, outfile)
    print('SAVED: data/trajectory.json')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def pointDistance(a, b):
    v = [b[0] - a[0], b[1] - a[1]]
    return math.sqrt(v[0]*v[0] + v[1]*v[1])

def vectorLen(v):
    return math.sqrt(v[0]*v[0] + v[1]*v[1])

def getVector(a, b):
    dis = pointDistance(a,b)
    return [(b[0] - a[0])/dis, (b[1] - a[1])/dis]

def normalizeVector(v):
    dis = vectorLen(v)
    return [v[0]/dis, v[1]/dis]

def getOrientedAngle(v1, v2):
    cross = v1[0]*v2[1] - v1[1]*v2[0]
    dot = v1[0]*v2[0] - v1[1]*v2[1]
    return math.atan2(cross, dot)
    
def distanceToPath(x, y):
    minDistance = None
    distance = None
    for curve in roadData:
        if(curve["type"] == "line"):
            distFromStart = pointDistance([x, y], curve["start"])
            distFromEnd = pointDistance([x, y], curve["end"])
            lineLength = pointDistance(curve["start"], curve["end"])

            # zatial len približne
            if(distFromStart > lineLength or distFromEnd > lineLength):
                angle = getOrientedAngle(getVector(curve["start"], curve["end"]), getVector(curve["start"], [x, y]))
                mark = -angle/math.fabs(angle) if (angle != 0) else 1
                distance = mark*min(distFromStart, distFromEnd)
            else:
                vector = getVector(curve["start"], curve["end"])
                a, b = vector[1], -vector[0]
                c = a*curve["start"][0] - b*curve["start"][1]
                distance = (a*x+b*y+c)/math.sqrt(a*a+b*b)

        elif(curve["type"] == "circle"):
            angle = math.atan2(y - curve["center"][1], x - curve["center"][0])
            if(angle > curve["start"] and  angle < curve["end"]):
                distance =(math.sqrt(math.pow(x - curve["center"][0], 2)+math.pow(y - curve["center"][1], 2)) - curve["radius"])
            else:
                distance = math.inf

        if(minDistance == None):
            minDistance = distance
        else:
            if(math.fabs(minDistance) > math.fabs(distance)):
                minDistance = distance

    return minDistance



def randRange(number):
    return random.randrange(number)

def getAngele(v1, v2):
    return math.acos((v1[0]*v2[0] + v1[1]*v2[1])/vectorLen(v1)* vectorLen(v2))

def crossover(traj1, traj2):
    if(random.uniform(0,1) < pCrossOver):
        dataLength = len(traj1)
        crossOverPoint = random.randrange(dataLength)

        for i in range(dataLength):
            if i < crossOverPoint:
                temp = traj1[i]
                traj1[i] = traj2[i]
                traj2[i] = temp
    return mutate(traj1), mutate(traj2)

def mutate(traj):
    for i in range(len(traj)):
        if(random.uniform(0,1) < pMutation):
            fi = random.triangular(-mutationRate, mutationRate)
            if((fi+traj[i]["beta"] < 0.14) and (traj[i]["gama"] + fi < 0.384)):
                traj[i]["beta"]  += fi
    return traj    

def getBackWheelPos(frontPos, alpha):
    x = frontPos[0] + wheelDistance*math.cos(alpha+math.pi)
    y = frontPos[1] + wheelDistance*math.sin(alpha+math.pi)
    return [x, y]

def getNextPoint(frontPos, alpha0, gama, beta, t, backPos):
    if(beta + gama == 0):
        direction = getVector(backPos, frontPos)
        xf = frontPos[0] + speed*t * direction[0]
        yf = frontPos[1] + speed*t * direction[1]

        xb = backPos[0] + speed*t * direction[0]
        yb = backPos[1] + speed*t * direction[1]

        return {"position": [xf, yf], "alpha": alpha0, "beta": 0, "gama": 0, "backWheelPos": [xb, yb] }
    else:
        radiusFront = wheelDistance / math.sin(math.fabs(beta + gama))
        radiusBack = radiusFront * math.cos(math.fabs(beta + gama))

        orientation = (beta/math.fabs(beta)) if (beta != 0) else 1
        omegaBack = speed/radiusBack
        fiBack = omegaBack*t*orientation
        alpha = alpha0 + fiBack

        omegaFront = speed/radiusFront
        fiFront = omegaFront*t*orientation

        cx = backPos[0] + radiusBack*math.cos(alpha0+math.pi/2*orientation)
        cy = backPos[1] + radiusBack*math.sin(alpha0+math.pi/2*orientation)
   
        x = ((frontPos[0] - cx)*math.cos(fiFront)-(frontPos[1]-cy)*math.sin(fiFront))+cx
        y = ((frontPos[0] - cx)*math.sin(fiFront)+(frontPos[1]-cy)*math.cos(fiFront))+cy

        betaMark = -1 if (beta >= 0 and gama >= 0) or (beta <= 0 and gama <= 0) else 1
         
        return {"position": [x, y], "alpha": alpha, "gama": gama + beta + betaMark*fiBack, "beta": beta, "backWheelPos": getBackWheelPos([x, y], alpha)}

def recalculate(traj):
    mistake = distanceToPath(startPoint[0], startPoint[1])
    fitness = 0
    trajectory = [{"position": startPoint, "alpha": 0, "beta": 0, "gama": 0, "backWheelPos": getBackWheelPos(startPoint, 0) }]
    for i in range(1, len(traj)):
        prevData = trajectory[-1]
        newData = getNextPoint(prevData["position"], prevData["alpha"], prevData["gama"], traj[i]["beta"], deltaTime, prevData["backWheelPos"])
        trajectory.append(newData)
        mistake += math.fabs(distanceToPath(newData["position"][0], newData["position"][1]))

        if(i > 1):
            prevPrevData = trajectory[i-2]
            vector1 = [prevData["position"][0] - prevPrevData["position"][0],prevData["position"][1] - prevPrevData["position"][1]]
            vector2 = [newData["position"][0] - prevData["position"][0], newData["position"][1] - prevData["position"][1]]
            angle = getOrientedAngle(vector1, vector2)
            if i*deltaTime < pointDistance(roadData[0]["start"], roadData[0]["end"])/speed - eps:
                properAngle = 0
            elif i*deltaTime > pointDistance(roadData[0]["start"], roadData[0]["end"])/speed + eps:
                properAngle = speed * deltaTime / roadData[1]["radius"]
            else:
                properAngle = speed * deltaTime / roadData[1]["radius"] / 2
            fitness += math.fabs(angle - properAngle)
            #print(i, angle, properAngle)
                
    return trajectory, mistake/len(traj), fitness/len(traj)

def getTrajectoryPoints(traj):
    front = []
    back = []
    distances = []
    for t in traj:
        front.append(t["position"])
        back.append(t["backWheelPos"])
        distances.append(distanceToPath(t["position"][0], t["position"][1]))
    return front, back, distances

def getRandomBeta(distance):
    beta = random.triangular(-4, 4)
    if distance > 2:
        beta += random.uniform(0, 4)
    elif distance < -2:
        beta += random.uniform(-4, 0)
    return beta*math.pi/180


fitScores = [None]*populationSize
properFitScores = [None]*populationSize
population = [None]*populationSize
bestScores = []
eps = deltaTime/2

for a in range(populationSize):
    mistake = distanceToPath(startPoint[0], startPoint[1])
    fitness = 0
    trajectory = [{"position": startPoint, "alpha": 0, "beta": 0, "gama": 0 , "backWheelPos": getBackWheelPos(startPoint, 0) }]

    for i in range(trajectoryLenght):
        prevData = trajectory[-1]
        dist = distanceToPath(prevData["position"][0], prevData["position"][1])
        
        beta = getRandomBeta(dist)
        while(prevData["gama"] + beta > 0.384):
            beta = getRandomBeta(dist)

        newData = getNextPoint(prevData["position"], prevData["alpha"], prevData["gama"], beta, deltaTime, prevData["backWheelPos"])
        trajectory.append(newData)
        mistake += math.fabs(distanceToPath(newData["position"][0], newData["position"][1]))
        if(i > 1):
            prevPrevData = trajectory[i-2]
            vector1 = [prevData["position"][0] - prevPrevData["position"][0],prevData["position"][1] - prevPrevData["position"][1]]
            vector2 = [newData["position"][0] - prevData["position"][0], newData["position"][1] - prevData["position"][1]]
            angle = getOrientedAngle(vector1, vector2)

            if i*deltaTime < pointDistance(roadData[0]["start"], roadData[0]["end"])/speed - eps:
                fitness += math.fabs(angle)
            elif i*deltaTime > pointDistance(roadData[0]["start"], roadData[0]["end"])/speed + eps:
                fitness += math.fabs(angle - speed * deltaTime / roadData[1]["radius"])
            else:
                fitness += math.fabs(angle - speed * deltaTime / roadData[1]["radius"] / 2)
                
            
    properFitScores[a] = fitness/len(trajectory)   
    fitScores[a] = mistake/len(trajectory)
    population[a] = trajectory

bestTrajectoryIndex = 0
for p in range(generationCount):
    mutationRate = mutationRateStart + p/generationCount*(mutationRateEnd - mutationRateStart)
    newPopulation = [population[bestTrajectoryIndex],population[bestTrajectoryIndex]]
    newFitScores = [fitScores[bestTrajectoryIndex], fitScores[bestTrajectoryIndex]]
    newProperFitScores = [properFitScores[bestTrajectoryIndex], properFitScores[bestTrajectoryIndex]]
    bestTrajectoryIndex = 0
    
    for i in range(2, populationSize, 2):
        scoreCount = len(fitScores)
        traj1id, traj2id = 0,0
        while(traj1id == traj2id):
            index1 = randRange(scoreCount)
            index2 = randRange(scoreCount)
            traj1id = index1 if (properFitScores[index1] < properFitScores[index2]) else index2
            index1 = randRange(scoreCount)
            index2 = randRange(scoreCount)
            traj2id = index1 if (properFitScores[index1] < properFitScores[index2]) else index2

        traj1, traj2 = crossover(list(population[traj1id]), list(population[traj2id]))
        newTrajectory1, fitScore1, properFitScores1 = recalculate(traj1)
        newTrajectory2, fitScore2, properFitScores2 = recalculate(traj2)
        newPopulation.append(newTrajectory1)
        newPopulation.append(newTrajectory2)
        newFitScores.append(fitScore1)
        newFitScores.append(fitScore2)
        newProperFitScores.append(properFitScores1)
        newProperFitScores.append(properFitScores2)
        if(newProperFitScores[bestTrajectoryIndex] > properFitScores1):
            bestTrajectoryIndex = i
        if(newProperFitScores[bestTrajectoryIndex] > properFitScores2):
            bestTrajectoryIndex = i+1
    population = newPopulation
    fitScores = newFitScores
    properFitScores = newProperFitScores

    bestScores.append({"generation": p, "distanceScore": min(fitScores), "angleScore": min(properFitScores)})
    print(p, min(fitScores), min(properFitScores))

    fitterstScore = fitScores.index(min(fitScores))
    frontTraj, backTraj, distances = getTrajectoryPoints(population[fitterstScore])
    with open('./../data/trajectory.json', 'w') as outfile:
        json.dump({"trajectoryData": frontTraj, "backTrajectoryData": backTraj, "distances": distances}, outfile)


fitterstScore = fitScores.index(min(fitScores))
frontTraj, backTraj, distances = getTrajectoryPoints(population[fitterstScore])

print('Best fit:', min(fitScores))
with open('./../data/trajectory.json', 'w') as outfile:
    json.dump({"trajectoryData": frontTraj, "backTrajectoryData": backTraj, "distances": distances}, outfile)

with open('./../data/scores.json', 'w') as outfile:
    json.dump({"scores": bestScores}, outfile)

print('SAVED: data/trajectory.json')


    

    
